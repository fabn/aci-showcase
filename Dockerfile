FROM openjdk:11-slim-buster as build
WORKDIR /workspace/app
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
RUN ./mvnw -B -q dependency:go-offline
COPY src src
RUN ./mvnw -B -q package -DskipTests
# Second stage
FROM openjdk:11-jre-slim-buster
RUN mkdir /app
WORKDIR /app
COPY --from=build /workspace/app/target/*.jar /app/app.jar
CMD ["java", "-jar", "/app/app.jar"]
