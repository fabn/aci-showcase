import logo from './logo.svg';
import './App.css';
import useSWR from 'swr'

const fetcher = (...args) => fetch(...args).then(res => res.json())

function ProductList() {
  // const {data, error} = useSWR('https://spring.fabn.ga/products', fetcher)
  const {data, error} = useSWR('/products', fetcher)

  if (error) return <div>failed to load</div>
  if (!data) return <div>loading...</div>

  // render data
  return <div>
    <h2>Lista prodotti</h2>
    <ul>
      {data.map(p => <li key={p.id}>{p.name} - {p.ean}</li>)}
    </ul>
  </div>
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h2>Deploy con gitlab e kustomize</h2>
        <ProductList/>
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
